# Install

build and install the trinity source binary with

```
go get gitlab.com/trinity0/trinity-source
```

make a script that sets up the env vars to source

```
export TRINITY_SOURCES=/path/to/trinity-source-packs
export TRINITY_DOWNLOADS=/path/to/trinity-source-downloads
```

source it, mkdir

```
. source
mkdir $TRINITY_DOWNLOADS
```

test the tool

```
trinity-source m4
```

download everything

```
./dl.sh
```