package main

import (
	"fmt"
	"os"
	"path"

	"log"
	"io"
	"bufio"
	
	"regexp"

	"os/exec"
	"crypto/sha256"
)

var re = regexp.MustCompile(`([^:]*):[[:space:]]+(.*)`)

func parseFile(path string) map[string]string {
	var mp = make(map[string]string)
	
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		m := re.FindStringSubmatch(scanner.Text())
		if m == nil {
			log.Fatal("Match error on", scanner.Text())
		}
		
		mp[m[1]] = m[2]
	}
	
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return mp
}

func download(dir string, link string, filename string) {
	if _, err := os.Stat(filename); err == nil {
		fmt.Println("File already exists, not re-downloading.")
		return
	}

	path, _ := exec.LookPath("wget")
	cmd := exec.Cmd{
		Path: path,
		Args: []string{"wget", link, "-O", filename},
		Dir: dir,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}
	
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func hash(filename string) []byte {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	
	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	
	return h.Sum(nil)
}

func main() {
	if len(os.Args) != 2 {
		panic("argc")
	}

	lfs_sources, exists := os.LookupEnv("TRINITY_SOURCES")
	if !exists {
		panic("TRINITY_SOURCES")
	}

	lfs_downloads, exists := os.LookupEnv("TRINITY_DOWNLOADS")
	if !exists {
		panic("TRINITY_DOWNLOADS")
	}

	pack := os.Args[1]
	fmt.Printf("Getting pack \"%v\"\n", pack)

	srcpath := path.Join(lfs_sources, pack + ".src")
	fmt.Printf("from location \"%v\"\n", srcpath)
	
	mp := parseFile(srcpath)
	link, exists := mp["download"]
	if !exists {
		fmt.Println(mp)
		panic("no download link!")
	}
	output, exists := mp["output"]
	if !exists {
		output = path.Base(link)
	}
	filename := path.Join(lfs_downloads, output)
	fmt.Printf("downloading <%v> as %v...\n", link, output)
	
	download(lfs_downloads, link, filename)
	hsh := hash(filename)
	
	fmt.Printf("hash: %x\n", hsh)
	hsh_check, exists := mp["hash"]
	if !exists {
		fmt.Printf("Warning: No hash found in %v\n", srcpath)
	} else {
		if hsh_check == fmt.Sprintf("%x", hsh) {
			fmt.Println("Hash validated successfully\n")
		} else {
			panic("DANGER: Invalid hash!")
		}
	}

	fmt.Println("")
}
