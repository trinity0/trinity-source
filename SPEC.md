The tool `trinity-source` downloads and checks source code packages based on .src description files.
If the source code has already been download it just checks the hash instead of redownloading.

The .src description files are written in a line based key: value format

download: <url>
output: <filename>
signature: <url>
hash: <sha256sum>

the output filename is derived from the download url, unless output is specified to override that.

gpg signature checking is not implemented yet.
